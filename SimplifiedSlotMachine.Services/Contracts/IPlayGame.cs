﻿using SimplifiedSlotMachine.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Services.Contracts
{
    interface IPlayGame
    {
        string Play(Game model, IRNDCalculator calculator, IGenerator generator);
    }
}
