﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Services.Contracts
{
    interface IRequestTranslator
    {
        string[] Parse(string commandLine);
    }
}
