﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Services.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}
