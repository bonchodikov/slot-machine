﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplifiedSlotMachine.Services.Contracts
{
    interface IGenerator
    {   
        int[] Generate(int minRange, int maxRange, int numberOfColomns);        
    }
}
