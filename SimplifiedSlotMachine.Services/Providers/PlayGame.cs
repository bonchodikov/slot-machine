﻿using SimplifiedSlotMachine.Data.Models;
using SimplifiedSlotMachine.Services.Contracts;
using System;
using System.Linq;
using System.Text;

namespace SimplifiedSlotMachine.Services.Providers
{
    class PlayGame : IPlayGame
    {
        private const int ROWS = 4;
        private const int COLS = 3;
        private const int MIN = 0;
        private const int MAX = 100;


        public string Play(Game model, IRNDCalculator calculator, IGenerator generator)
        {
            var isFirstSpin = true;
            while (model.Balance > 0)
            {
                if (!isFirstSpin)
                {
                    Console.Write("Please enter your bet:" + '\t');
                    model.Bet = decimal.Parse(Console.ReadLine());
                    if (model.Bet < 1)
                    {
                        throw new ArgumentException("Bet must be bigger than 1");
                    }
                }
                if (model.Balance < model.Bet)
                {
                    Console.WriteLine("Please enter bet smaller or equal of your balance.");
                    continue;
                }

                isFirstSpin = false;
                var balance = model.Balance;
                var bet = model.Bet;
                decimal totalWins = 0;
                if (model.Balance < model.Bet)
                {
                    throw new Exception("Not enough funds!");
                }

                var combinations = new int[ROWS][];

                for (int i = 0; i < ROWS; i++)
                {
                    var currentRow = calculator.GetGameNumbers(generator.Generate(MIN, MAX, COLS));
                    combinations[i] = currentRow;
                    if (calculator.ContainsDuplicates(currentRow))
                    {
                        totalWins += calculator.CalculateWinCoeficient(currentRow);
                    }
                }
                totalWins *= model.Bet;
                model.Balance = calculator.CalculateCurrentBalance(balance, model.Bet, totalWins);
                model.RoundDifference = totalWins;
                string result = String.Join(Environment.NewLine,combinations
                                                        .Select(items => String.Join(" ", items)));

                Console.WriteLine();
                var bldr = new StringBuilder(result);
                bldr.Replace("45", "A");
                bldr.Replace("35", "B");
                bldr.Replace("15", "P");
                bldr.Replace("5", "*");

                Console.WriteLine(bldr.ToString());

                bldr.Clear();

                bldr.AppendLine("You have won:" + '\t' + totalWins);
                bldr.AppendLine("Current balance is:" + '\t' + model.Balance);
                Console.WriteLine(bldr.ToString());
            }

            return null;
        }

    }
}
