﻿using Ninject;
using SimplifiedSlotMachine.Services.Contracts;
using System;
using System.Reflection;

namespace SimplifiedSlotMachine.Services
{
    public class Engine : IEngine
    {
        private const string Delimiter = "####################";
        private readonly IRequestTranslator parser;
        private readonly IFactory factory;
        private readonly IRNDCalculator calculator;
        private readonly IGenerator generator;
        private readonly IPlayGame play;

        static Engine()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            Instance = new Engine(kernel);
        }

        private Engine(StandardKernel kernel)
        {
            this.parser = kernel.Get<IRequestTranslator>();
            this.factory = kernel.Get<IFactory>();
            this.calculator = kernel.Get<IRNDCalculator>();
            this.generator = kernel.Get<IGenerator>();
            this.play = kernel.Get<IPlayGame>();
        }

        public void Run()
        {
            while (true)
            {
                // Read -> Process -> Print -> Repeat
                Console.WriteLine("Please enter Initial amount and your bet. For Example: 200 10");
                string input = this.Read();
                string result = this.Process(input);
                this.Print(result);
            }
        }

        public static IEngine Instance { get; }

        private string Read()
        {
            return Console.ReadLine();
        }

        private string Process(string commandLine)
        {
            if (commandLine == "end")
            {
                Environment.Exit(0);
            }

            try
            {                              
                var game = this.factory.Create(this.parser.Parse(commandLine));
                var result = this.play.Play(game, this.calculator, this.generator);

                if (result==null)
                {
                    return "Do you want to play another game!? Enter a balance and your bet, or enter end to close the app";
                }

                return result;                             
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return $"ERROR: {e.Message}";
            }
        }

        private void Print(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
